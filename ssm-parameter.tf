resource "aws_ssm_parameter" "msp" {
  for_each = local.enabled_ssm_parameters

  name        = each.value.name
  type        = each.value.type
  value       = each.value.value
  description = each.value.description
  overwrite   = true

  tags = local.tags_module
}
