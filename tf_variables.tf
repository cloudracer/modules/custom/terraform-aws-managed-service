/*
Generic
*/
variable "region" {
  description = "The region of the account"
  type        = string
  default     = ""
}

variable "cloud_management_account_id" {
  description = "Account ID for tecRacer MSP Cloud Monitoring"
  type        = number
  default     = null
}

variable "cloud_management_external_id" {
  description = "External ID for tecRacer MSP Cloud Monitoring"
  type        = string
  default     = ""
}

variable "cloud_monitoring_account_id" {
  description = "Account ID for tecRacer MSP Monitoring of Site24x7"
  type        = number
  default     = null
}

variable "cloud_monitoring_external_id" {
  description = "External ID for tecRacer MSP Monitoring of Site24x7"
  type        = string
  default     = ""
}

variable "disabled_ssm_parameters" {
  description = "List of IDs of disabled SSM Parameters"
  type        = list(string)
  default     = []
}

variable "disabled_cloudwatch_loggroups" {
  description = "List of IDs of disabled CloudWatch Log Groups"
  type        = list(string)
  default     = []
}

variable "tags" {
  description = "A map of tags to add to all resources"
  type        = map(string)
  default     = {}
}

variable "SMParameterLogBucket" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: S3 LogBucket Name"
}

variable "SSMParameterSSMLogBucket" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: S3 SSM-LogBucket Name"
}

variable "SSMParameterDaysToGlacier" {
  type        = number
  default     = 60
  description = "tecRacer Managed-Services - Managed SSM Parameter: S3 retention days until glacier"
}

variable "SSMParameterDaysToDelete" {
  type        = number
  default     = 180
  description = "tecRacer Managed-Services - Managed SSM Parameter: S3 retention days until deleted"
}

variable "SSMParameterConfigSubscriber" {
  type        = string
  default     = "https://api.opsgenie.com/v1/json/amazonsns?apiKey=08cfcd2d-d2e7-4854-8802-1d662a0c3dc5"
  description = "tecRacer Managed-Services - Managed SSM Parameter: AWS-Config to OpsGenie subscriber (HTTPS)"
}

variable "SSMParameterCloudAWSBackupCronDaily" {
  type        = string
  default     = "cron(30 21 ? * 2,3,4,5,6,7 *)"
  description = "tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Daily CRON schedule"
}

variable "SSMParameterCloudAWSBackupCronWeekly" {
  type        = string
  default     = "cron(40 21 ? * 1 *)"
  description = "tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Weekly CRON schedule"
}

variable "SSMParameterCloudAWSBackupCronMonthly" {
  type        = string
  default     = "cron(50 21 ? * 2#1 *)"
  description = "tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Monthly CRON schedule"
}

variable "SSMParameterCloudAWSBackupOption" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Option"
}

variable "SSMParameterCloudInventoryOption" {
  type        = string
  default     = "no"
  description = "tecRacer Managed-Services - Managed SSM Parameter: CloudInventory Option"
}

variable "SSMParameterCloudInventoryCron" {
  type        = string
  default     = "cron(10 22 ? * 1 *)"
  description = "tecRacer Managed-Services - Managed SSM Parameter: SSM Inventory CRON schedule"
}

variable "SSMParameterCloudManagementExternalid" {
  type        = string
  default     = "CC-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
  description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Management External ID"
}

variable "SSMParameterCloudMonitoringAccount" {
  type        = string
  default     = "949777495771"
  description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Monitoring Account ID"
}

variable "SSMParameterCloudMonitoringExternalId" {
  type        = string
  default     = "SITE24x7-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
  description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Monitoring External ID"
}

variable "SSMParameterCloudTrendMicroOption" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: Trend Micro Option"
}

variable "SSMParameterCloudBitDefenderOption" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: BitDefender Option"
}

variable "cloud_bitdefender_account_id" {
  type        = string
  default     = "446478696618"
  description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud BitDefender Account"
}

variable "cloud_bitdefender_external_id" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud BitDefender External ID"
}

variable "cloud_trendmicro_account_id" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Trend-Micro Account ID"
}

variable "cloud_trendmicro_external_id" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Trend-Micro External ID"
}

variable "SSMParameterCloudParkMyCloudOption" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud Option"
}

variable "parkmycloud_account_id" {
  type        = string
  default     = "753542375798"
  description = "tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud Account ID"
}

variable "parkmycloud_external_id" {
  type        = string
  default     = "PARKMYCLOUD-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
  description = "tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud External ID"
}

variable "SSMParameterCloudWatchLogsConfigWindows" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Windows"
}

variable "SSMParameterCloudWatchLogsConfigUbuntu" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Ubuntu"
}

variable "SSMParameterCloudWatchLogsConfigDebian" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Debian"
}

variable "SSMParameterCloudPatchingOption" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Option"
}

variable "SSMParameterCloudPatchingPatchgroupTagNameWin" {
  type        = string
  default     = "Win<OS Version>Prod"
  description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Patchgroup Tag Name-Windows"
}

variable "SSMParameterCloudPatchingPatchgroupTagNameLin" {
  type        = string
  default     = "Lin<OS Version>Prod"
  description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Patchgroup Tag Name-Linux"
}

variable "SSMParameterCloudPatchingCustomerName" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Customer Name"
}

variable "SSMParameterCloudPatchingWindowsCRON" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Windows CRON schedule"
}

variable "SSMParameterCloudPatchingLinuxCRON" {
  type        = string
  default     = "null"
  description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Linux CRON schedule"
}
