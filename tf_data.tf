data "aws_iam_policy" "cloud_monitoring_read_only_access" {
  arn = "arn:aws:iam::aws:policy/ReadOnlyAccess"
}
