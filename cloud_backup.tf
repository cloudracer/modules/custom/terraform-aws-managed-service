#resource "aws_iam_role" "cloud_backup_role" {
#  count = var.cloud_backup_account_id != "" ? 1 : 0
#
#  name        = "CloudMonitoringIAMRole"
#  description = "tecRacer Managed-Services - Managed CAA Role for CloudMonitoring"
#
#  assume_role_policy = <<-EOF
#  {
#    "Version": "2012-10-17",
#    "Statement": [
#      {
#        "Action": "sts:AssumeRole",
#        "Principal": {
#          "AWS": "arn:aws:iam::${var.cloud_backup_account_id}:root"
#        },
#        "Effect": "Allow",
#        "Sid": "",
#      }
#    ]
#  }
#  EOF
#
#  tags = merge(var.tags, local.tags, {})
#}
#
#resource "aws_iam_role_policy" "cloud_backup_01" {
#  count = var.parkmycloud_account_id != "null" && var.parkmycloud_external_id != "null" ? 1 : 0
#
#  name = "CloudBackupCore"
#  role = aws_iam_role.cloud_backup_role[0].id
#
#  #tfsec:ignore:aws-iam-no-policy-wildcards
#  #tfsec:ignore:aws-iam-block-kms-policy-wildcard
#  policy = <<EOF
#{
#  "Version": "2012-10-17",
#  "Statement": [
#    {
#      "Action": [
#        "iam:SimulatePrincipalPolicy",
#        "iam:GetAccountAuthorizationDetails",
#        "iam:ListUsers",
#        "iam:ListAccessKeys",
#        "iam:GetAccountAuthorizationDetails",
#        "iam:ListUserPolicies",
#        "iam:ListAttachedRolePolicies",
#        "iam:ListAttachedUserPolicies",
#        "iam:ListAttachedGroupPolicies",
#        "iam:ListGroupPolicies",
#        "iam:ListGroupsForUser",
#        "iam:GetRole",
#        "ec2:CopySnapshot",
#        "ec2:CreateSnapshot",
#        "ec2:CreateSnapshots",
#        "ec2:CreateTags",
#        "ec2:DeleteSnapshot",
#        "ec2:DescribeAddresses",
#        "ec2:DescribeAvailabilityZones",
#        "ec2:DescribeImageAttribute",
#        "ec2:DescribeImages",
#        "ec2:DescribeInstanceAttribute",
#        "ec2:DescribeInstanceStatus",
#        "ec2:DescribeInstances",
#        "ec2:DescribeKeyPairs",
#        "ec2:DescribePlacementGroups",
#        "ec2:DescribeRegions",
#        "ec2:DescribeSecurityGroups",
#        "ec2:DescribeSnapshotAttribute",
#        "ec2:DescribeSnapshots",
#        "ec2:DescribeSubnets",
#        "ec2:DescribeTags",
#        "ec2:DescribeVolumeAttribute",
#        "ec2:DescribeVolumeStatus",
#        "ec2:DescribeVolumes",
#        "ec2:ModifySnapshotAttribute",
#        "ec2:ResetSnapshotAttribute",
#        "ec2:CreateImage",
#        "ec2:CopyImage",
#        "ec2:DescribeVpcs",
#        "ec2:ModifyImageAttribute",
#        "ec2:RegisterImage",
#        "ec2:DeregisterImage",
#        "ec2:AuthorizeSecurityGroupEgress",
#        "ec2:AuthorizeSecurityGroupIngress",
#        "rds:DescribeDBInstances",
#        "rds:DescribeDBClusters",
#        "rds:DescribeDBClusterParameterGroups",
#        "rds:DescribeDBClusterParameters",
#        "rds:DescribeDBClusterSnapshots",
#        "rds:DescribeDBParameterGroups",
#        "rds:DescribeDBParameters",
#        "rds:DescribeDBSnapshots",
#        "rds:DescribeDBSubnetGroups",
#        "rds:DescribeOptionGroups",
#        "redshift:DescribeClusterParameterGroups",
#        "redshift:DescribeClusterParameters",
#        "redshift:DescribeClusterSnapshots",
#        "redshift:DescribeClusterSubnetGroups",
#        "redshift:DescribeClusters",
#        "redshift:DescribeTags",
#        "dynamodb:DescribeTimeToLive",
#        "dynamodb:DescribeTable",
#        "dynamodb:DescribeBackup",
#        "dynamodb:ListTables",
#        "dynamodb:ListTagsOfResource",
#        "dynamodb:CreateTable",
#        "dynamodb:ListBackups",
#        "dynamodb:Scan",
#        "kms:DescribeKey",
#        "kms:ListKeys",
#        "kms:ListAliases",
#        "kms:CreateGrant",
#        "kms:GenerateDataKeyWithoutPlaintext",
#        "sts:AssumeRole",
#        "cloudwatch:GetMetricStatistics",
#        "ec2:DescribeNetworkInterfaces",
#        "ec2:AttachNetworkInterface",
#        "ec2:DescribeNetworkInterfaceAttribute",
#        "ec2:DescribeNetworkInterfacePermissions",
#        "ses:SendRawEmail",
#        "ses:ListVerifiedEmailAddresses",
#        "ses:VerifyEmailAddress",
#        "ses:GetIdentityVerificationAttributes",
#        "ssm:GetParametersByPath"
#      ],
#      "Effect": "Allow",
#      "Resource": "*"
#    }
#  ]
#}
#EOF
#
#  depends_on = [aws_iam_role.cloud_backup_role]
#}
