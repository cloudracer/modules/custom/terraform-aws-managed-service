#tfsec:ignore:aws-cloudwatch-log-group-customer-key
resource "aws_cloudwatch_log_group" "msp" {
  for_each = local.enabled_cloudwatch_loggroups

  name              = each.value.name
  retention_in_days = each.value.retention_in_days

  tags = local.tags_module
}
