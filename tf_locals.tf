locals {
  available_ssm_parameters = {
    SMParameterLogBucket = {
      name        = "/account/logs/bucketname"
      type        = "String"
      value       = "null"
      description = "tecRacer Managed-Services - Managed SSM Parameter: S3 LogBucket Name"
    }

    SSMParameterSSMLogBucket = {
      name        = "/account/logs/ssmbucketname"
      type        = "String"
      value       = var.SSMParameterSSMLogBucket
      description = "tecRacer Managed-Services - Managed SSM Parameter: S3 SSM-LogBucket Name"
    }

    SSMParameterDaysToGlacier = {
      name        = "/account/logs/daystoglacier"
      type        = "String"
      value       = var.SSMParameterDaysToGlacier
      description = "tecRacer Managed-Services - Managed SSM Parameter: S3 retention days until glacier"
    }

    SSMParameterDaysToDelete = {
      name        = "/account/logs/daystodelete"
      type        = "String"
      value       = "null"
      description = "tecRacer Managed-Services - Managed SSM Parameter: S3 retention days until deleted"
    }

    SSMParameterConfigSubscriber = {
      name        = "/account/logs/subscriber"
      type        = "String"
      value       = var.SSMParameterDaysToDelete
      description = "tecRacer Managed-Services - Managed SSM Parameter: AWS-Config to OpsGenie subscriber (HTTPS)"
    }

    SSMParameterCloudAWSBackupCronDaily = {
      name        = "/account/awsbackup/crondaily"
      type        = "String"
      value       = var.SSMParameterCloudAWSBackupCronDaily
      description = "tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Daily CRON schedule"
    }

    SSMParameterCloudAWSBackupCronWeekly = {
      name        = "/account/awsbackup/cronweekly"
      type        = "String"
      value       = var.SSMParameterCloudAWSBackupCronWeekly
      description = "tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Weekly CRON schedule"
    }

    SSMParameterCloudAWSBackupCronMonthly = {
      name        = "/account/awsbackup/cronmonthly"
      type        = "String"
      value       = var.SSMParameterCloudAWSBackupCronMonthly
      description = "tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Monthly CRON schedule"
    }

    SSMParameterCloudAWSBackupOption = {
      name        = "/account/awsbackup/enabled"
      type        = "String"
      value       = var.SSMParameterCloudAWSBackupOption
      description = "tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Option"
    }

    SSMParameterCloudInventoryOption = {
      name        = "/account/inventory/enabled"
      type        = "String"
      value       = var.SSMParameterCloudInventoryOption
      description = "tecRacer Managed-Services - Managed SSM Parameter: CloudInventory Option"
    }

    SSMParameterCloudInventoryCron = {
      name        = "/account/inventory/cron"
      type        = "String"
      value       = var.SSMParameterCloudInventoryCron
      description = "tecRacer Managed-Services - Managed SSM Parameter: SSM Inventory CRON schedule"
    }

    SSMParameterCloudManagementExternalid = {
      name        = "/account/cloudmgmt/externalid"
      type        = "String"
      value       = var.SSMParameterCloudManagementExternalid
      description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Management External ID"
    }

    SSMParameterCloudMonitoringAccount = {
      name        = "/account/monitoring/accountid"
      type        = "String"
      value       = var.SSMParameterCloudMonitoringAccount
      description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Monitoring Account ID"
    }

    SSMParameterCloudMonitoringExternalId = {
      name        = "/account/monitoring/externalid"
      type        = "String"
      value       = var.SSMParameterCloudMonitoringExternalId
      description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Monitoring External ID"
    }

    SSMParameterCloudTrendMicroOption = {
      name        = "/account/trendmicro/enabled"
      type        = "String"
      value       = var.SSMParameterCloudTrendMicroOption
      description = "tecRacer Managed-Services - Managed SSM Parameter: Trend Micro Option"
    }

    SSMParameterCloudBitDefenderOption = {
      name        = "/account/bitdefender/enabled"
      type        = "String"
      value       = var.SSMParameterCloudBitDefenderOption
      description = "tecRacer Managed-Services - Managed SSM Parameter: BitDefender Option"
    }

    SSMParameterCloudBitDefenderAccountid = {
      name        = "/account/bitdefender/accountid"
      type        = "String"
      value       = var.cloud_bitdefender_account_id
      description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud BitDefender Account ID"
    }

    SSMParameterCloudBitDefenderExternalid = {
      name        = "/account/bitdefender/externalid"
      type        = "String"
      value       = var.cloud_bitdefender_external_id
      description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud BitDefender External ID"
    }

    SSMParameterCloudTrendMicroAccountid = {
      name        = "/account/trendmicro/accountid"
      type        = "String"
      value       = var.cloud_trendmicro_account_id
      description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Trend-Micro Account ID"
    }

    SSMParameterCloudTrendMicroExternalid = {
      name        = "/account/trendmicro/externalid"
      type        = "String"
      value       = var.cloud_trendmicro_external_id
      description = "tecRacer Managed-Services - Managed SSM Parameter: Cloud Trend-Micro External ID"
    }

    SSMParameterCloudParkMyCloudOption = {
      name        = "/account/parkmycloud/enabled"
      type        = "String"
      value       = var.SSMParameterCloudParkMyCloudOption
      description = "tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud Option"
    }

    SSMParameterParkMyCloudAccount = {
      name        = "/account/parkmycloud/accountid"
      type        = "String"
      value       = var.parkmycloud_account_id
      description = "tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud Account ID"
    }

    SSMParameterParkMyCloudExternalId = {
      name        = "/account/parkmycloud/externalid"
      type        = "String"
      value       = var.parkmycloud_external_id
      description = "tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud External ID"
    }

    SSMParameterCloudWatchLogsConfigWindows = {
      name        = "/account/cloudwatchlogsconfig/windows"
      type        = "String"
      value       = var.SSMParameterCloudWatchLogsConfigWindows
      description = "tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Windows"
    }

    SSMParameterCloudWatchLogsConfigUbuntu = {
      name        = "/account/cloudwatchlogsconfig/ubuntu"
      type        = "String"
      value       = var.SSMParameterCloudWatchLogsConfigUbuntu
      description = "tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Ubuntu"
    }

    SSMParameterCloudWatchLogsConfigDebian = {
      name        = "/account/cloudwatchlogsconfig/debian"
      type        = "String"
      value       = var.SSMParameterCloudWatchLogsConfigDebian
      description = "tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Debian"
    }

    SSMParameterCloudPatchingOption = {
      name        = "/account/patching/enabled"
      type        = "String"
      value       = var.SSMParameterCloudPatchingOption
      description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Option"
    }

    SSMParameterCloudPatchingPatchgroupTagNameWin = {
      name        = "/account/patching/patchgrouptagwin"
      type        = "String"
      value       = var.SSMParameterCloudPatchingPatchgroupTagNameWin
      description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Patchgroup Tag Name-Windows"
    }

    SSMParameterCloudPatchingPatchgroupTagNameLin = {
      name        = "/account/patching/patchgrouptaglin"
      type        = "String"
      value       = var.SSMParameterCloudPatchingPatchgroupTagNameLin
      description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Patchgroup Tag Name-Linux"
    }

    SSMParameterCloudPatchingCustomerName = {
      name        = "/account/patching/customername"
      type        = "String"
      value       = var.SSMParameterCloudPatchingCustomerName
      description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Customer Name"
    }

    SSMParameterCloudPatchingWindowsCRON = {
      name        = "/account/patching/windowscron"
      type        = "String"
      value       = var.SSMParameterCloudPatchingWindowsCRON
      description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Windows CRON schedule"
    }

    SSMParameterCloudPatchingLinuxCRON = {
      name        = "/account/patching/linuxcron"
      type        = "String"
      value       = var.SSMParameterCloudPatchingLinuxCRON
      description = "tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Linux CRON schedule"
    }
  }
  enabled_ssm_parameters = { for k, v in local.available_ssm_parameters : k => v if !contains(var.disabled_ssm_parameters, k) }

  available_cloudwatch_loggroups = {

    CloudWatchLogGroupForCloudTrail = {
      name              = "_CloudTrailLogs"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWL = {
      name              = "_CloudWatchLogs"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCloudMaintenance = {
      name              = "_CloudMaintenanceLogs"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLWindowsSystem = {
      name              = "_CloudWatchLogs/_Windows/Windows-SystemEventLog"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLWindowsApplication = {
      name              = "_CloudWatchLogs/_Windows/Windows-ApplicationEventLog"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxMessages = {
      name              = "_CloudWatchLogs/_Linux/var/log/messages"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxSyslog = {
      name              = "_CloudWatchLogs/_Linux/var/log/syslog"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxCron = {
      name              = "_CloudWatchLogs/_Linux/var/log/cron.log"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxSecure = {
      name              = "_CloudWatchLogs/_Linux/var/log/secure"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxDPKG = {
      name              = "_CloudWatchLogs/_Linux/var/log/dpkg.log"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxAptitude = {
      name              = "_CloudWatchLogs/_Linux/var/log/aptitude"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxAwslogs = {
      name              = "_CloudWatchLogs/_Linux/var/log/awslogs.log"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxAuth = {
      name              = "_CloudWatchLogs/_Linux/var/log/auth.log"
      retention_in_days = "180"
    }

    CloudWatchLogGroupForCWLLinuxFailtoBan = {
      name              = "_CloudWatchLogs/_Linux/var/log/fail2ban.log"
      retention_in_days = "180"
    }
  }
  enabled_cloudwatch_loggroups = { for k, v in local.available_cloudwatch_loggroups : k => v if !contains(var.disabled_cloudwatch_loggroups, k) }

  tags_module = {
    Terraform                = true
    Terraform_Module         = "terraform-aws-msp-stack"
    Terraform_Module_Source  = "https://gitlab.com/tecracer-intern/terraform-landingzone/modules/terraform-aws-msp-stack"
    tecRacer-ManagedServices = true
  }
  tags = merge(local.tags_module, var.tags)
}
