resource "aws_iam_role" "cloud_bitdefender_role" {
  count = var.cloud_bitdefender_account_id != "null" && var.cloud_bitdefender_external_id != "null" ? 1 : 0

  name        = "CloudTrendMicro"
  description = "tecRacer Managed-Services - Managed CAA Role for CloudBitDefender"

  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "AWS": "arn:aws:iam::${var.cloud_bitdefender_account_id}:root"
        },
        "Effect": "Allow",
        "Sid": "",
        "Condition": {
          "StringEquals": {"sts:ExternalId":"${var.cloud_bitdefender_external_id}"}
        }
      }
    ]
  }
  EOF

  tags = local.tags_module
}
