resource "aws_iam_role" "cloud_trendmicro" {
  count = var.cloud_trendmicro_account_id != "null" && var.cloud_trendmicro_external_id != "null" ? 1 : 0

  name        = "CloudTrendMicro"
  description = "tecRacer Managed-Services - Managed CAA Role for CloudTrendMicro"

  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "AWS": "arn:aws:iam::${var.cloud_trendmicro_account_id}:root"
        },
        "Effect": "Allow",
        "Sid": "",
        "Condition": {
          "StringEquals": {"sts:ExternalId":"${var.cloud_trendmicro_external_id}"}
        }
      }
    ]
  }
  EOF

  tags = local.tags_module
}

resource "aws_iam_policy_attachment" "cloud_trendmicro" {
  count = var.cloud_trendmicro_account_id != "null" && var.cloud_trendmicro_external_id != "null" ? 1 : 0

  name       = aws_iam_role.cloud_trendmicro[0].name
  policy_arn = aws_iam_policy.cloud_trendmicro[0].arn
}

resource "aws_iam_policy" "cloud_trendmicro" {
  count = var.cloud_trendmicro_account_id != "null" && var.cloud_trendmicro_external_id != "null" ? 1 : 0

  policy = data.aws_iam_policy_document.cloud_trendmicro[0].json
}

data "aws_iam_policy_document" "cloud_trendmicro" {
  count = var.cloud_trendmicro_account_id != "null" && var.cloud_trendmicro_external_id != "null" ? 1 : 0

  statement {
    effect = "Allow"

    actions = [
      "ec2:DescribeImages",
      "ec2:DescribeInstances",
      "ec2:DescribeRegions",
      "ec2:DescribeSubnets",
      "ec2:DescribeTags",
      "ec2:DescribeVpcs",
      "ec2:DescribeAvailabilityZones",
      "ec2:DescribeSecurityGroups",
      "workspaces:DescribeWorkspaces",
      "workspaces:DescribeWorkspaceDirectories",
      "workspaces:DescribeWorkspaceBundles",
      "workspaces:DescribeTags",
      "iam:ListAccountAliases",
      "iam:GetRole",
      "iam:GetRolePolicy"
    ]

    resources = ["*"]
  }
}
