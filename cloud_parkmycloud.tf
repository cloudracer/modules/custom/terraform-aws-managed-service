resource "aws_iam_role" "park_my_cloud_role" {
  count = var.parkmycloud_account_id != "null" && var.parkmycloud_external_id != "null" ? 1 : 0

  name        = "CloudParkMyCloud"
  description = "tecRacer Managed-Services - Managed CAA Role for CloudParkMyCloud"

  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "AWS": "arn:aws:iam::${var.parkmycloud_account_id}:root"
        },
        "Effect": "Allow",
        "Sid": "",
        "Condition": {
          "StringEquals": {"sts:ExternalId":"${var.parkmycloud_external_id}"}
        }
      }
    ]
  }
  EOF

  tags = local.tags_module
}

resource "aws_iam_role_policy" "park_my_cloud_policy_00" {
  count = var.parkmycloud_account_id != "null" && var.parkmycloud_external_id != "null" ? 1 : 0

  name = "ParkMyCloudRecommendedPolicy"
  role = aws_iam_role.park_my_cloud_role[0].id

  #tfsec:ignore:aws-iam-no-policy-wildcards
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "autoscaling:Describe*",
        "autoscaling:ResumeProcesses",
        "autoscaling:SuspendProcesses",
        "autoscaling:UpdateAutoScalingGroup",
        "ce:Describe*",
        "ce:Get*",
        "ce:List*",
        "ec2:Describe*",
        "ec2:ModifyInstanceAttribute",
        "ec2:StartInstances",
        "ec2:StopInstances",
        "ecs:Describe*",
        "ecs:List*",
        "ecs:UpdateService",
        "eks:Describe*",
        "eks:List*",
        "eks:UpdateNodegroupConfig",
        "iam:GetUser",
        "redshift:Describe*",
        "redshift:PauseCluster",
        "redshift:ResizeCluster",
        "redshift:ResumeCluster",
        "rds:Describe*",
        "rds:ListTagsForResource",
        "rds:ModifyDBInstance",
        "rds:StartDBCluster",
        "rds:StartDBInstance",
        "rds:StopDBCluster",
        "rds:StopDBInstance",
        "savingsplans:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  depends_on = [aws_iam_role.park_my_cloud_role]
}

resource "aws_iam_role_policy" "park_my_cloud_policy_01" {
  count = var.parkmycloud_account_id != "null" && var.parkmycloud_external_id != "null" ? 1 : 0

  name = "ParkMyCloudStartInstanceWithEncryptedBoot"
  role = aws_iam_role.park_my_cloud_role[0].id

  #tfsec:ignore:aws-iam-no-policy-wildcards
  #tfsec:ignore:aws-iam-block-kms-policy-wildcard
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "kms:CreateGrant"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  depends_on = [aws_iam_role.park_my_cloud_role]
}

resource "aws_iam_role_policy" "park_my_cloud_policy_02" {
  count = var.parkmycloud_account_id != "null" && var.parkmycloud_external_id != "null" ? 1 : 0

  name = "ParkMyCloudCloudWatchAccess"
  role = aws_iam_role.park_my_cloud_role[0].id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "cloudwatch:GetMetricStatistics",
        "cloudwatch:ListMetrics"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  depends_on = [aws_iam_role.park_my_cloud_role]
}
