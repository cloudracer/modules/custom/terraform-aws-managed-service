<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 3.0 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.63.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_log_group.msp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_log_group) | resource |
| [aws_iam_policy.cloud_trendmicro](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy) | resource |
| [aws_iam_policy_attachment.cloud_trendmicro](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy_attachment) | resource |
| [aws_iam_role.cloud_bitdefender_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.cloud_management_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.cloud_monitoring_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.cloud_trendmicro](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role.park_my_cloud_role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy.iam_billing](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.iam_cloudtrail](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.iam_cost](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.iam_flowlogs](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.iam_inventory](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.iam_securitycompliance](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.park_my_cloud_policy_00](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.park_my_cloud_policy_01](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy.park_my_cloud_policy_02](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy) | resource |
| [aws_iam_role_policy_attachment.cloud_monitoring_policy_attachment](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_ssm_parameter.msp](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/ssm_parameter) | resource |
| [aws_iam_policy.cloud_monitoring_read_only_access](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy) | data source |
| [aws_iam_policy_document.cloud_trendmicro](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_SMParameterLogBucket"></a> [SMParameterLogBucket](#input\_SMParameterLogBucket) | tecRacer Managed-Services - Managed SSM Parameter: S3 LogBucket Name | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudAWSBackupCronDaily"></a> [SSMParameterCloudAWSBackupCronDaily](#input\_SSMParameterCloudAWSBackupCronDaily) | tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Daily CRON schedule | `string` | `"cron(30 21 ? * 2,3,4,5,6,7 *)"` | no |
| <a name="input_SSMParameterCloudAWSBackupCronMonthly"></a> [SSMParameterCloudAWSBackupCronMonthly](#input\_SSMParameterCloudAWSBackupCronMonthly) | tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Monthly CRON schedule | `string` | `"cron(50 21 ? * 2#1 *)"` | no |
| <a name="input_SSMParameterCloudAWSBackupCronWeekly"></a> [SSMParameterCloudAWSBackupCronWeekly](#input\_SSMParameterCloudAWSBackupCronWeekly) | tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Weekly CRON schedule | `string` | `"cron(40 21 ? * 1 *)"` | no |
| <a name="input_SSMParameterCloudAWSBackupOption"></a> [SSMParameterCloudAWSBackupOption](#input\_SSMParameterCloudAWSBackupOption) | tecRacer Managed-Services - Managed SSM Parameter: AWSBackup Option | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudBitDefenderOption"></a> [SSMParameterCloudBitDefenderOption](#input\_SSMParameterCloudBitDefenderOption) | tecRacer Managed-Services - Managed SSM Parameter: BitDefender Option | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudInventoryCron"></a> [SSMParameterCloudInventoryCron](#input\_SSMParameterCloudInventoryCron) | tecRacer Managed-Services - Managed SSM Parameter: SSM Inventory CRON schedule | `string` | `"cron(10 22 ? * 1 *)"` | no |
| <a name="input_SSMParameterCloudInventoryOption"></a> [SSMParameterCloudInventoryOption](#input\_SSMParameterCloudInventoryOption) | tecRacer Managed-Services - Managed SSM Parameter: CloudInventory Option | `string` | `"no"` | no |
| <a name="input_SSMParameterCloudManagementExternalid"></a> [SSMParameterCloudManagementExternalid](#input\_SSMParameterCloudManagementExternalid) | tecRacer Managed-Services - Managed SSM Parameter: Cloud Management External ID | `string` | `"CC-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"` | no |
| <a name="input_SSMParameterCloudMonitoringAccount"></a> [SSMParameterCloudMonitoringAccount](#input\_SSMParameterCloudMonitoringAccount) | tecRacer Managed-Services - Managed SSM Parameter: Cloud Monitoring Account ID | `string` | `"949777495771"` | no |
| <a name="input_SSMParameterCloudMonitoringExternalId"></a> [SSMParameterCloudMonitoringExternalId](#input\_SSMParameterCloudMonitoringExternalId) | tecRacer Managed-Services - Managed SSM Parameter: Cloud Monitoring External ID | `string` | `"SITE24x7-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"` | no |
| <a name="input_SSMParameterCloudParkMyCloudOption"></a> [SSMParameterCloudParkMyCloudOption](#input\_SSMParameterCloudParkMyCloudOption) | tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud Option | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudPatchingCustomerName"></a> [SSMParameterCloudPatchingCustomerName](#input\_SSMParameterCloudPatchingCustomerName) | tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Customer Name | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudPatchingLinuxCRON"></a> [SSMParameterCloudPatchingLinuxCRON](#input\_SSMParameterCloudPatchingLinuxCRON) | tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Linux CRON schedule | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudPatchingOption"></a> [SSMParameterCloudPatchingOption](#input\_SSMParameterCloudPatchingOption) | tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Option | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudPatchingPatchgroupTagNameLin"></a> [SSMParameterCloudPatchingPatchgroupTagNameLin](#input\_SSMParameterCloudPatchingPatchgroupTagNameLin) | tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Patchgroup Tag Name-Linux | `string` | `"Lin<OS Version>Prod"` | no |
| <a name="input_SSMParameterCloudPatchingPatchgroupTagNameWin"></a> [SSMParameterCloudPatchingPatchgroupTagNameWin](#input\_SSMParameterCloudPatchingPatchgroupTagNameWin) | tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Patchgroup Tag Name-Windows | `string` | `"Win<OS Version>Prod"` | no |
| <a name="input_SSMParameterCloudPatchingWindowsCRON"></a> [SSMParameterCloudPatchingWindowsCRON](#input\_SSMParameterCloudPatchingWindowsCRON) | tecRacer Managed-Services - Managed SSM Parameter: CloudPatching Windows CRON schedule | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudTrendMicroOption"></a> [SSMParameterCloudTrendMicroOption](#input\_SSMParameterCloudTrendMicroOption) | tecRacer Managed-Services - Managed SSM Parameter: Trend Micro Option | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudWatchLogsConfigDebian"></a> [SSMParameterCloudWatchLogsConfigDebian](#input\_SSMParameterCloudWatchLogsConfigDebian) | tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Debian | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudWatchLogsConfigUbuntu"></a> [SSMParameterCloudWatchLogsConfigUbuntu](#input\_SSMParameterCloudWatchLogsConfigUbuntu) | tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Ubuntu | `string` | `"null"` | no |
| <a name="input_SSMParameterCloudWatchLogsConfigWindows"></a> [SSMParameterCloudWatchLogsConfigWindows](#input\_SSMParameterCloudWatchLogsConfigWindows) | tecRacer Managed-Services - Managed SSM Parameter: AmazonCloudWatchLogs-Agent config for Windows | `string` | `"null"` | no |
| <a name="input_SSMParameterConfigSubscriber"></a> [SSMParameterConfigSubscriber](#input\_SSMParameterConfigSubscriber) | tecRacer Managed-Services - Managed SSM Parameter: AWS-Config to OpsGenie subscriber (HTTPS) | `string` | `"https://api.opsgenie.com/v1/json/amazonsns?apiKey=08cfcd2d-d2e7-4854-8802-1d662a0c3dc5"` | no |
| <a name="input_SSMParameterDaysToDelete"></a> [SSMParameterDaysToDelete](#input\_SSMParameterDaysToDelete) | tecRacer Managed-Services - Managed SSM Parameter: S3 retention days until deleted | `number` | `180` | no |
| <a name="input_SSMParameterDaysToGlacier"></a> [SSMParameterDaysToGlacier](#input\_SSMParameterDaysToGlacier) | tecRacer Managed-Services - Managed SSM Parameter: S3 retention days until glacier | `number` | `60` | no |
| <a name="input_SSMParameterSSMLogBucket"></a> [SSMParameterSSMLogBucket](#input\_SSMParameterSSMLogBucket) | tecRacer Managed-Services - Managed SSM Parameter: S3 SSM-LogBucket Name | `string` | `"null"` | no |
| <a name="input_cloud_bitdefender_account_id"></a> [cloud\_bitdefender\_account\_id](#input\_cloud\_bitdefender\_account\_id) | tecRacer Managed-Services - Managed SSM Parameter: Cloud BitDefender Account | `string` | `"446478696618"` | no |
| <a name="input_cloud_bitdefender_external_id"></a> [cloud\_bitdefender\_external\_id](#input\_cloud\_bitdefender\_external\_id) | tecRacer Managed-Services - Managed SSM Parameter: Cloud BitDefender External ID | `string` | `"null"` | no |
| <a name="input_cloud_management_account_id"></a> [cloud\_management\_account\_id](#input\_cloud\_management\_account\_id) | Account ID for tecRacer MSP Cloud Monitoring | `number` | `null` | no |
| <a name="input_cloud_management_external_id"></a> [cloud\_management\_external\_id](#input\_cloud\_management\_external\_id) | External ID for tecRacer MSP Cloud Monitoring | `string` | `""` | no |
| <a name="input_cloud_monitoring_account_id"></a> [cloud\_monitoring\_account\_id](#input\_cloud\_monitoring\_account\_id) | Account ID for tecRacer MSP Monitoring of Site24x7 | `number` | `null` | no |
| <a name="input_cloud_monitoring_external_id"></a> [cloud\_monitoring\_external\_id](#input\_cloud\_monitoring\_external\_id) | External ID for tecRacer MSP Monitoring of Site24x7 | `string` | `""` | no |
| <a name="input_cloud_trendmicro_account_id"></a> [cloud\_trendmicro\_account\_id](#input\_cloud\_trendmicro\_account\_id) | tecRacer Managed-Services - Managed SSM Parameter: Cloud Trend-Micro Account ID | `string` | `"null"` | no |
| <a name="input_cloud_trendmicro_external_id"></a> [cloud\_trendmicro\_external\_id](#input\_cloud\_trendmicro\_external\_id) | tecRacer Managed-Services - Managed SSM Parameter: Cloud Trend-Micro External ID | `string` | `"null"` | no |
| <a name="input_disabled_cloudwatch_loggroups"></a> [disabled\_cloudwatch\_loggroups](#input\_disabled\_cloudwatch\_loggroups) | List of IDs of disabled CloudWatch Log Groups | `list(string)` | `[]` | no |
| <a name="input_disabled_ssm_parameters"></a> [disabled\_ssm\_parameters](#input\_disabled\_ssm\_parameters) | List of IDs of disabled SSM Parameters | `list(string)` | `[]` | no |
| <a name="input_parkmycloud_account_id"></a> [parkmycloud\_account\_id](#input\_parkmycloud\_account\_id) | tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud Account ID | `string` | `"753542375798"` | no |
| <a name="input_parkmycloud_external_id"></a> [parkmycloud\_external\_id](#input\_parkmycloud\_external\_id) | tecRacer Managed-Services - Managed SSM Parameter: ParkMyCloud External ID | `string` | `"PARKMYCLOUD-XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"` | no |
| <a name="input_region"></a> [region](#input\_region) | The region of the account | `string` | `""` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to add to all resources | `map(string)` | `{}` | no |

## Outputs

No outputs.
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
