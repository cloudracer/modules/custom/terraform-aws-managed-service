resource "aws_iam_role" "cloud_monitoring_role" {
  count = var.cloud_monitoring_account_id != "" && var.cloud_monitoring_external_id != "" ? 1 : 0

  name        = "CloudMonitoringIAMRole"
  description = "tecRacer Managed-Services - Managed CAA Role for CloudMonitoring"

  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
        "Action": "sts:AssumeRole",
        "Principal": {
          "AWS": "arn:aws:iam::${var.cloud_monitoring_account_id}:root"
        },
        "Effect": "Allow",
        "Sid": "",
        "Condition": {
          "StringEquals": {"sts:ExternalId":"${var.cloud_monitoring_external_id}"}
        }
      }
    ]
  }
  EOF

  tags = merge(var.tags, local.tags, {})
}

resource "aws_iam_role_policy_attachment" "cloud_monitoring_policy_attachment" {
  count = var.cloud_monitoring_account_id != "" && var.cloud_monitoring_external_id != "" ? 1 : 0

  role       = aws_iam_role.cloud_monitoring_role[0].name
  policy_arn = data.aws_iam_policy.cloud_monitoring_read_only_access.arn
}
